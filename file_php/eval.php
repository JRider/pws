<?php
class evaluate_table {
	var $table;
	var $todays_date, 
		$vento, 
		$barometro, 
		$umidita, 
		$temperatura, 
		$data, 
		$ventoDir,
		$pioggia,
		$nomepws; 


	public function evaluate_table($table) {
		$this->todays_date = date("Y-m-d");
		$this->table = $table;
		$this->temperatura = $table[4];
		$this->umidita = $table[5];
		$this->barometro = $table[6];
		$this->pioggia = $table[7];
		$this->nomepws = $table[32];
		$this->vento = $table[1] * 1.852;
		$this->ventoDir = $this->calcolaVento($table[3]);

		$this->data = $table[74];

	}
	
	private function calcolaVento($windDir){
		if($windDir == 0){return '0';}
		if($windDir > 0 && $windDir <= 15){return '15'; }
		if($windDir > 15 && $windDir <= 30){return '30'; }
		if($windDir > 30 && $windDir <= 45){return '45'; }
		if($windDir > 45 && $windDir <= 60){return '60'; }
		if($windDir > 60 && $windDir <= 75){return '75'; }
		if($windDir > 75 && $windDir <= 90){return '90'; }
		if($windDir > 90 && $windDir <= 105){return '105'; }
		if($windDir > 105 && $windDir <= 120){return '120'; }
		if($windDir > 120 && $windDir <= 135){return '135'; }
		if($windDir > 135 && $windDir <= 150){return '150'; }
		if($windDir > 150 && $windDir <= 165){return '165'; }
		if($windDir > 165 && $windDir <= 180){return '180'; }
		if($windDir > 180 && $windDir <= 195){return '195'; }
		if($windDir > 195 && $windDir <= 210){return '210'; }
		if($windDir > 210 && $windDir <= 225){return '225'; }
		if($windDir > 225 && $windDir <= 240){return '240'; }
		if($windDir > 240 && $windDir <= 255){return '255'; }
		if($windDir > 255 && $windDir <= 270){return '270'; }
		if($windDir > 270 && $windDir <= 285){return '285'; }
		if($windDir > 285 && $windDir <= 300){return '300'; }
		if($windDir > 300 && $windDir <= 315){return '315'; }
		if($windDir > 315 && $windDir <= 330){return '330'; }
		if($windDir > 330 && $windDir <= 345){return '345'; }
		if($windDir > 345 ){return '0'; }
		
	}
}


?>