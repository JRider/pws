<?php
include 'file_php/session.inc';
include ("file_php/connect.php");
include ("file_php/userQuery.php");
include ("file_php/eval.php");
connect ();
$row_ut = userQuery ();
?>
<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<title>PWS - Private Weather Station</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/homepage.css" type="text/css">
	<link rel="stylesheet" href="css/mytemplate.css" type="text/css">
	<link rel="stylesheet" href="css/weather-icons.css" type="text/css">
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
	<?php
	$file = fopen ( $_SESSION ['pws'], "r" ) or exit ( "Unable to open file!" );
			// http://quartuweather.altervista.org/
			// Output a line of the file until the end is reached
	while ( ! feof ( $file ) ) {

		$plik = fgets ( $file );
	}
	fclose ( $file );
	$array = explode ( " ", $plik );
	$test = new evaluate_table ( $array );
	?>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
				class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">PWS</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="homepage.php">Home</a></li>
				<li><a href="about.html">About</a></li>

			</ul>
			<ul class="nav navbar-nav pull-right">
				<li><a href="file_php/logout.php" class="pull-right">Logout</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>

<script type="text/javascript">
$(document).ready(function() {
	var object = new myobject();
	
	object.interval = setInterval(aggiorna, 15000,object);
});
function myobject()
{
	this.i = 0;
	this.interval = 0;
	
}
	function aggiorna(object) { 
		if(object.i==10){
			stopFunction(object);

			return;	
		}

		$.ajax({
			url: "file_php/aggiorna.php",
			dataType: "json",
			method: "POST",
			data: {aggiorna: 'true'},

			success: function(data){
				object.i=object.i+1;
				if($(".temp").text() != data.temperatura){
					$(".temp").html(data.temperatura).css( "color", "aqua" );
				}
				if($(".vento").text() != data.vento){
					$(".vento").html(data.vento).css( "color", "aqua" );

				}
				setTimeout(function(){$("span").each(function(){this.removeAttribute("style");});}, 8000);
			},
			error: function(error){
				object.i=object.i+1;
				alert( "Errore di aggiornamento " + error );

			}
		});


	}
	function stopFunction(object){
		clearInterval(object.interval);
	}
	

</script>
<div class="container info">

	<div class="row">
		<h1 class="pull-center pwsname">
			<strong>Benvenuto a <?php echo $test->nomepws;?></strong> 
			<a href="javascript: var object = new myobject(); aggiorna(object);"  class="pull-right"><i class="wi wi-cloud-refresh medium"></i></a>
		</h1>
	</div>

	<div class="row">
		<div class="col-md-5 col-md-offset-1 block">

			<div class="pull-center">
				<div class="row title">
					<div class="col-md-5 col-md-offset-1  ">
						<h1>Oggi</h1> 
					</div>
					<div class="col-md-2 col-md-offset-3  ">
						<h1><span class="pull-right" ><?php echo $test->data;?></span></h1>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-md-3 col-md-offset-1 ">
						<h1>
							<i class="wi wi-day-lightning"></i>
						</h1>
					</div>
					<div class="col-md-6 col-md-offset-1 ">
						<div id="box" class="altriDati">
							<div id="temp"><i class="wi wi-thermometer big"></i>
								<span class="temp big"><?php echo $test->temperatura;?></span>
								<i class="wi wi-celsius big"></i>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

		<div class="col-md-3 col-md-offset-2 block">
			<div class="pull-center">
				<div class="row title">
					<div class="col-md-5 col-md-offset-1">
						<h1>Vento</h1>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<div id='windir'>
							<i class="wi wi-wind-default _<?php echo $test->ventoDir;?>-deg big" id='windDir'></i> 
						</div>
					</div>
					<div class="col-md-7">
						<div id='wind' class="medium"> <span class="vento"><?php echo $test->vento;?> km/h</span></div>
					</div>

				</div>
			</div>
		</div>

	</div>
	<div class="row"><br></div>
	<div class="row">

		<div class="col-md-4 col-md-offset-1 block">

			<div class="row title">
				<div class="col-md-5 col-md-offset-1">
					<h1>Pioggia</h1>
				</div>
			</div>
			<div class="row"><br></div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">
					<i class="wi wi-sprinkles big"></i>
				</div>
				<div class="col-md-5 col-md-offset-1 medium">
					<?php echo $test->pioggia;?>mm 
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1 medium">
					<div id='um'>Umidit&agrave; </div>
				</div>
				<div class="col-md-5 col-md-offset-1 medium">
					<?php echo $test->umidita;?>%
				</div>
			</div>
		</div>


		<div class="col-md-4 col-md-offset-2 block">
			<div class="pull-center">

				<div class="row title">
					<div class="col-md-11 col-md-offset-1  ">
						<h2>Barometro</h2>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-md-5 col-md-offset-1 ">

						<i class="wi wi-down big "></i>
					</div>
					<div class="col-md-5 col-md-offset-1 medium">
						<?php echo $test->barometro;?>hPa</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>



	<div class="footer">
	<div class="row">
		<div class="col-md-4 col-md-offset-5 ">
	
		<div>
			<a href=" http://validator.w3.org/check/referer" class="gray">W3C HTML</a> <a
			href=" http://jigsaw.w3.org/css-validator/check/refer" class="gray" >W3C Css</a>
		</div>
		<div class="row-fluid gray">
			<p>&copy;Creato da Julia Schmidt</p>
		</div>
		</div>
		</div>
	</div>
</body>
</html>
